package com.barpoltech.freesms;
import org.litepal.LitePal;
import java.util.Date;
import java.util.List;

public class MessageDAL {

    public static List<Message> getMessages() {
        return LitePal.findAll(Message.class);
    }

    public static void addMessage(String message, String phone) {
        Message m = new Message();
        m.setMessage(message);
        m.setPhone(phone);
        m.setSent(new Date());
        m.save();

        if(LitePal.count(Message.class) >= 50) {
            Message oldest = LitePal.order("sent").limit(1).find(Message.class).get(0);
            LitePal.delete(Message.class, oldest.getId());
        }
    }
}
