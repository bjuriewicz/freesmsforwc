package com.barpoltech.freesms;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class QRScanner {

    Activity activity;
    MyPreferences prefs;

    public QRScanner(Activity a) {
        activity = a;
        prefs = new MyPreferences(activity);
    }

    public void scanQRToken() {
        IntentIntegrator integrator = new IntentIntegrator(activity);
        integrator.setBeepEnabled(false);
        integrator.setOrientationLocked(false);
        integrator.initiateScan();
    }

    public Boolean handleQRScanResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            String qrToken = result.getContents();
            if(qrToken != null) {
                if (qrToken.length() != 32) {
                    showErrorDialog();
                } else {
                    prefs.set("qrToken", qrToken);
                    new RegisterUtil(activity).registerTokens(prefs);
                }

                return true;
            }
            return false;
        }
        return false;
    }

    private void showErrorDialog() {
        SweetAlertDialog pDialog = new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE);
        pDialog.setTitleText("Whoops!")
                .setContentText("Make sure you have scanned the correct QR code (the one on Free SMS for Woocommerce config page), and you're connected to the internet.")
                .show();
    }
}
