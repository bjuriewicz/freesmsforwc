package com.barpoltech.freesms;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;
import org.litepal.LitePal;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class MainActivity extends AppCompatActivity {

    QRScanner scanner;
    MyPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LitePal.initialize(this);
        scanner = new QRScanner(this);
        preferences = new MyPreferences(this);
        checkPermissions();
        initView();
    }

    private void initView() {
        setContentView(R.layout.activity_main);

        ListView messagesListView = findViewById(R.id.messagesListView);
        messagesListView.setAdapter(new MessagesListAdapter(this, MessageDAL.getMessages()));

        messagesListView.setEmptyView(findViewById(R.id.emptyList));

        Button scanQRButton = findViewById(R.id.scanQRButton);
        scanQRButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scanner.scanQRToken();
            }
        });

        ImageButton refreshButton = findViewById(R.id.refreshButton);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initView();
            }
        });



        if (preferences.get("qrToken").isEmpty()) {
            showInitMessage();
        } else {
            scanQRButton.setText("Scan new QR Code");
        }
    }

    private void checkPermissions() {
        PermissionsAndServicesChecker permissionsAndServicesChecker = new PermissionsAndServicesChecker(this);
        permissionsAndServicesChecker.GooglePlayCheck();
        permissionsAndServicesChecker.PermissionsCheck();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Boolean res = scanner.handleQRScanResult(requestCode, resultCode, data);
        if(!res) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length == 0) {
                    Toast.makeText(MainActivity.this, "You must allow camera access to scan the QR code.", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    private void showInitMessage() {
        SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.NORMAL_TYPE);
        pDialog.setTitleText("Almost ready!")
                .setContentText("Now, open you Wordpress admin panel, go to Free SMS plugin settings and scan the QR code in top-right of the page.")
                .show();
    }
}